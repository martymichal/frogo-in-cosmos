extends Sprite2D

signal pressed

const TRANSPARENCY_MAX: float = 1
const TRANSPARENCY_JITTER: float = 0.15

func _ready() -> void:
	$TouchScreenButton.released.connect(emit_pressed)
	$Timer.timeout.connect(timer_shot)

func _physics_process(_delta: float) -> void:
	shimmer()

func shimmer() -> void:
	var color = self.self_modulate
	color.a = (max(randf_range(color.a - TRANSPARENCY_JITTER, TRANSPARENCY_MAX), 0))
	# TODO: Maybe change the size as well?
	# TODO: Maybe change the color as well?
	self.self_modulate = color

func emit_pressed() -> void:
	if $Timer.time_left != 0:
		return

	$GPUParticles2D.emitting = true
	pressed.emit()
	$Timer.start()

func timer_shot() -> void:
	$GPUParticles2D.emitting = false
