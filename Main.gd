extends Node

const COSMOS_HUES = { 
	'GREEN': Color(0.2,0.92,0.35),
	'BLUE': Color(0.71,0.90,1),
	'RED': Color(0.83,0.45,0.33),
	'ORANGE': Color(1,0.59,0),
	'PURPLE': Color(0.8,0,0.98),
	'YELLOW': Color(0.85,0.69,0),
}
const DIRECTIONS: Array[Vector2i] = [Vector2i.LEFT, Vector2i.RIGHT]
const HUE_TRANSITION_STEP: float = 0.01
const FROGO_ROTATION_MAX_LEFT: float = -24
const FROGO_ROTATION_MAX_RIGHT: float = 5
const FROGO_ROTATION_STEP: float = 0.015

var next_color: Color
var direction: Vector2i
var stream_playback: AudioStreamPlaybackPolyphonic

@onready var frogo = $CanvasLayer/Frogo
@onready var audio_stream_player = $MusicManager/AudioStreamPlayer

func _ready() -> void:
	next_color = $ParallaxBackground/CosmosHue.color
	direction = DIRECTIONS.pick_random()

	for star in $Stars.get_children():
		star.pressed.connect(star_pressed)

	frogo.play()
	
	#stream_playback = audio_stream_player.get_stream_playback()
	#var stream = AudioStreamOggVorbis.load_from_file('res://calm-piano-tomomi-kato.ogg')
	#stream.loop = true
	#stream_playback.play_stream(stream)

func _physics_process(_delta: float) -> void:
	var color = $ParallaxBackground/CosmosHue.color
	color.r = lerpf(color.r, next_color.r, HUE_TRANSITION_STEP)
	color.g = lerpf(color.g, next_color.g, HUE_TRANSITION_STEP)
	color.b = lerpf(color.b, next_color.b, HUE_TRANSITION_STEP)
	$ParallaxBackground/CosmosHue.color = color

	if direction == Vector2i.LEFT:
		frogo.rotation_degrees = lerpf(frogo.rotation_degrees, FROGO_ROTATION_MAX_LEFT, FROGO_ROTATION_STEP)

		if frogo.rotation_degrees <= FROGO_ROTATION_MAX_LEFT + 1:
			direction = Vector2i.RIGHT
	elif direction == Vector2i.RIGHT:
		frogo.rotation_degrees = lerpf(frogo.rotation_degrees, FROGO_ROTATION_MAX_RIGHT, FROGO_ROTATION_STEP)

		if frogo.rotation_degrees >= FROGO_ROTATION_MAX_RIGHT - 1:
			direction = Vector2i.LEFT

func star_pressed() -> void:
	var color: Color = COSMOS_HUES.values().pick_random()
	while color == next_color:
		color = COSMOS_HUES.values().pick_random()
	next_color = color
